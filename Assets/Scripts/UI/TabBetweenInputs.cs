using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class TabBetweenInputs : MonoBehaviour
{
    private static TMP_InputField userInput;
    private static TMP_InputField emailInput;
    private static TMP_InputField passInput;
    public int selectedInput;
    private InputAction m_tabPushed;           // Tab Action reference

    private void Start()
    {
        GameObject login = GameObject.Find("LoginRegister");
        if (login != null)
        {
            userInput = login.transform.Find("Username").GetComponent<TMP_InputField>();
            emailInput = GameObject.Find("Email").GetComponent<TMP_InputField>();
            passInput = GameObject.Find("Password").GetComponent<TMP_InputField>();
            EventSystem ev = GameObject.Find("EventSystem").GetComponent<EventSystem>();
            InputActionMap uiActionMap = ev.GetComponent<PlayerInput>().actions.FindActionMap("UI");
            uiActionMap.Enable();
            m_tabPushed = uiActionMap.FindAction("Tab");
            m_tabPushed.Enable();
            m_tabPushed.performed += SelectInputField;
        }
    }

    public void SelectInputField(InputAction.CallbackContext obj)
    {
        if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
        {
            selectedInput++;
            if (selectedInput > 2)
            {
                if (userInput.IsActive()) selectedInput = 0;
                else selectedInput = 1;
            }
            switch (selectedInput)
            {
                case 0:
                    userInput.Select();
                    break;
                case 1:
                    emailInput.Select();
                    break;
                case 2:
                    passInput.Select();
                    break;
            }
        }
    }
    public void UserSelected() => selectedInput = 0;
    public void EmailSelected() => selectedInput = 1;
    public void PasswordSelected() => selectedInput = 2;
}
