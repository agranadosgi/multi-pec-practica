using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.MultiplayerModels;
using System;
using Mirror;
using System.Net;
using System.Linq;
using PlayFab.Json;
using Complete;

public class MatchmakingManager : MonoBehaviour
{
    private bool   isTicketCreated = false;
    private bool   askForStatus = true;
    private string ticketNumber = "";
    private string queueName = "PEC4"; // Playfab name for queque name
    private string matchId = "";
    private string myPlayfabID;
    private string myPlayfabType;
    private float  timeToReload;
    private string ipServer = ""; // IP of player that makes server
    private LobbyUIManager lobbyUIManager;
    private float timeToStart;
    private bool isGameStart = false;

    public void Start()
    {
        timeToStart = 0f;
        GetAccountInfo();
        isTicketCreated = false;
        askForStatus = true;
        DontDestroyOnLoad(this.gameObject);
        lobbyUIManager = GameObject.Find("NetworkManager").GetComponentInChildren<LobbyUIManager>();    
    }

    private void OnEnable()
    {
        GameManager.OnPlayerStopGame += CancelMatchmakingTicket;
        GameManager.OnServerStopGame += CancelMatchmakingTicket;
    }

    private void OnDisable()
    {
        GameManager.OnPlayerStopGame -= CancelMatchmakingTicket;
        GameManager.OnServerStopGame -= CancelMatchmakingTicket;
    }

    // Matchmaking round life circle
    public void Update()
    {
        if (!isGameStart) { 
            timeToReload += Time.deltaTime;
            if (timeToReload > 6f)
            {
                askForStatus = true;
            }

            if (isTicketCreated && askForStatus)
            {
                askForStatus = false;
                timeToReload = 0;
                GetMatchId(ticketNumber, queueName);
            }
        }
        else
        {
            lobbyUIManager.SetDisplayStatus("");
        }
    }

    // Get info from player who connected to game
    public void GetAccountInfo()
    {
        GetAccountInfoRequest request = new GetAccountInfoRequest();
        PlayFabClientAPI.GetAccountInfo(request, OnGetIdSuccesss, OnMatchmakingError);
    }

    void OnGetIdSuccesss(GetAccountInfoResult result)
    {
        myPlayfabID = result.AccountInfo.TitleInfo.TitlePlayerAccount.Id;
        myPlayfabType = result.AccountInfo.TitleInfo.TitlePlayerAccount.Type;
    }

    // Create matchmaking ticket for all players
    // And set Ip from the workaround
    public void MatchmakingCreateTicket()
    {
        if (lobbyUIManager.CheckConnectionType() != 0)
        {
            // Server or host checking
            ipServer = lobbyUIManager.getPlayerIP();
        }
        PlayFabMultiplayerAPI.CreateMatchmakingTicket(new CreateMatchmakingTicketRequest
        {
            // Player attributes for matchmaking
            Creator = new MatchmakingPlayer
            {
                Entity = new PlayFab.MultiplayerModels.EntityKey
                {
                   Id = myPlayfabID,
                   Type = myPlayfabType
                },

                // matchmaking attributes
                Attributes = new MatchmakingPlayerAttributes
                {
                    DataObject = new
                    {
                        Skill = 24.4,
                        // Set IP from player making the server
                        IP = ipServer
                    },
                },
            },

            // Ticket life after stop the searching players to match
            GiveUpAfterSeconds = 120,

            // Queuename updated
            QueueName = queueName,
        },

    // Callbacks for messages
    this.OnMatchmakingTicketCreated,
    this.OnMatchmakingError);
    }

    private void OnMatchmakingTicketCreated(CreateMatchmakingTicketResult obj)
    {
        Debug.Log("Ticket creado correctamente");
        ticketNumber = obj.TicketId;
        isTicketCreated = true;
        Debug.Log("El ticket es: " + obj.TicketId);
        Debug.Log("La request es: " + obj.Request.ToJson());
    }

    // Check players and set on same queque game
    public void GetMatchId(string ticketId, string queue)
    {
        PlayFabMultiplayerAPI.GetMatchmakingTicket(new GetMatchmakingTicketRequest{
            TicketId = ticketId,
            QueueName = queue,
         },
        this.OnGetMatchmakingTicket,
        this.OnMatchmakingError);
    }

    private void OnGetMatchmakingTicket(GetMatchmakingTicketResult obj)
    {
        Debug.Log("El status es: " + obj.Status);
        if (obj.Status == "Matched")
        {
            if (!isGameStart) { 
                lobbyUIManager.SetDisplayStatus("Match found! Looking for players!");
            }
            else
            {
                lobbyUIManager.SetDisplayStatus("");
            }
            matchId = obj.MatchId;

            timeToStart += 1f;
            if(timeToStart >= 5f) { 
                GetMatch(matchId, queueName);
            }
        } else if(obj.Status == "Canceled")
        {
            isTicketCreated = false;
            lobbyUIManager.SetDisplayStatus("No players found");
            switch (lobbyUIManager.CheckConnectionType())
            {
                case 0:
                    CancelMatchmakingTicket();
                    break;
                case 1:
                    lobbyUIManager.StopHost();
                    break;
                case 2:
                    lobbyUIManager.StopServer();
                    break;
            }
        }
    }

    public void GetMatch(string matchId, string queue)
    {
        PlayFabMultiplayerAPI.GetMatch(new GetMatchRequest{
            MatchId = matchId,
            QueueName = queue,
            ReturnMemberAttributes = true,
        },
        this.OnGetMatch,
        this.OnMatchmakingError);
    }

    private void OnGetMatch(GetMatchResult obj)
    {
        lobbyUIManager.SetDisplayStatus("Starting match! Prepare your weapons!");
        Debug.Log("Se ha obtenido el listado de usuarios");
        Debug.Log(obj.ToJson());
        isGameStart = true;
        if (lobbyUIManager.CheckConnectionType() == 0 && !lobbyUIManager.CheckClientIsConnecting())
        {
            // client gets host IP
            string serverIP = "";
            foreach (var member in obj.Members)
            {
                if (member.Attributes != null && member.Attributes.DataObject != null)
                {
                    if (member.Attributes.DataObject is JsonObject dataDict)
                    {
                        if (dataDict.TryGetValue("IP", out object ipValueObj))
                        {
                            string ip = ipValueObj.ToString();
                            if (ip != "")
                            {
                                serverIP = ip;
                                Debug.Log("LA IP PÚBLICA ES: " + serverIP);
                                break;
                            }
                        }
                    }
                }
            }

            // Checking for debuging
            Debug.Log("La IP del host es: " + serverIP);
            lobbyUIManager.DirectConnection(serverIP);
        }
    }

    public void CancelMatchmakingTicket()
    {
        PlayFabMultiplayerAPI.CancelMatchmakingTicket(new CancelMatchmakingTicketRequest{
            QueueName = queueName,
            TicketId = ticketNumber,
        },
        this.OnTicketCanceled,
        this.OnMatchmakingError);
    }

    private void OnTicketCanceled(CancelMatchmakingTicketResult obj)
    {
        Debug.Log("El ticket se ha cancelado correctamente");
    }

    // Matchmaking code for players enter to the game
    public void OnJoinMatch()
    {
        PlayFabMultiplayerAPI.JoinMatchmakingTicket( new JoinMatchmakingTicketRequest {
            TicketId = ticketNumber,
            QueueName = queueName,
            Member = new MatchmakingPlayer
            {
                Entity = new PlayFab.MultiplayerModels.EntityKey
                {
                    Id = myPlayfabID,
                    Type = myPlayfabType,
                },
                Attributes = new MatchmakingPlayerAttributes
                {
                    DataObject = new
                    {
                        Skill = 19.3
                    },
                },
            }
    },
    this.OnJoinMatchmakingTicket,
    this.OnMatchmakingError);
    }

    private void OnJoinMatchmakingTicket(JoinMatchmakingTicketResult obj)
    {
        Debug.Log("OnJoinMatchmakingTicket" + obj.ToJson());
    }

    // Error callbacks messages
    private void OnMatchmakingError(PlayFabError obj)
    {
        Debug.Log("Se ha producido un error");
        Debug.Log(obj.ErrorMessage);
        Debug.Log(obj.ErrorDetails);
    }
}
