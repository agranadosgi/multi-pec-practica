using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RegisterLoginUI : MonoBehaviour
{
    private PlayFabController playFab => PlayFabController.Instance;

    [Header("Menus")]
    [SerializeField] private GameObject loginRegisterMenu;
    [SerializeField] private GameObject displayNameMenu;

    [Space(10f)]
    [SerializeField] private GameObject usernameInput;
    [SerializeField] private TMP_Text actionButtonText;
    [SerializeField] private TMP_Text secondaryButtonText;
    [SerializeField] private GameObject registerButton;
    [SerializeField] private TMP_Text errorMessage;

    private string username;
    private string email;
    private string password;
    private string displayName;

    private bool isLogin = true;

    private void OnEnable()
    {
        PlayFabController.OnShowLogin += ShowLoginRegister;
        PlayFabController.OnShowDisplayNameMenu += ShowDisplayNameMenu;
        PlayFabController.OnError += SetErrorText;
    }

    private void OnDisable()
    {
        PlayFabController.OnShowLogin -= ShowLoginRegister;
        PlayFabController.OnShowDisplayNameMenu -= ShowDisplayNameMenu;
        PlayFabController.OnError -= SetErrorText;
    }

    private void ShowLoginRegister()
    {
        loginRegisterMenu.SetActive(true);
    }

    private void ShowDisplayNameMenu()
    {
        loginRegisterMenu.SetActive(false);
        displayNameMenu.SetActive(true);
    }

    private void SetErrorText(string error)
    {
        errorMessage.text = error;
    }

    public void ToggleLoginRegister()
    {
        isLogin = !isLogin;

        actionButtonText.text = isLogin ? "Log In" : "Register";
        secondaryButtonText.text = isLogin ? "Register" : "Log In";

        usernameInput.SetActive(!isLogin);
    }

    public void SetUsername(string value)
    {
        username = value;
    }

    public void SetEmail(string value)
    {
        email = value;
    }

    public void SetPassword(string value)
    {
        password = value;
    }

    public void SetDisplayName(string value)
    {
        displayName = value;
    }

    public void RegisterLoginAction()
    {
        string errorMessage = "";

        if (isLogin)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
            {
                if (password.Length >= 6)
                {
                    playFab.LoginUser(email, password);
                }
                else
                {
                    errorMessage = "Password must contain at least 6 characters";
                }
            }
            else
            {
                errorMessage = "Fill all the fields";
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(username))
            {
                if (password.Length >= 6 && username.Length >= 3)
                {
                    playFab.RegisterUser(email, password, username);
                }
                else if (password.Length < 6)
                {
                    errorMessage = "Password must contain at least 6 characters";
                }
                else if (username.Length < 3)
                {
                    errorMessage = "Username must contain between 3 and 20 characters";
                }
            }
            else
            {
                errorMessage = "Fill all the fields";
            }
        }

        SetErrorText(errorMessage);
    }

    public void UpdateDisplayName()
    {
        if (!string.IsNullOrEmpty(displayName) && displayName.Length >= 3)
        {
            playFab.SetUserDisplayName(displayName);
        }
        else
        {
            SetErrorText("The display name must contain between 3 and 20 characters");
        }
    }
}
