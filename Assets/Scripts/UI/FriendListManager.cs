﻿using UnityEngine;
using System.Collections;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;
using System;
using TMPro;

public class FriendListManager : MonoBehaviour
{
    List<FriendInfo> _friends = null;
    public enum FriendIdType { PlayFabId, Username, Email, DisplayName };
    public TMPro.TMP_Dropdown friendDropdown;
    public TMPro.TMP_InputField friendInputField;
    public TMPro.TextMeshProUGUI friendsErrors;

    public void Start()
    {
        GetFriends();
    }

    public void AddFriendByEmail()
    {
        AddFriend(FriendIdType.Email);
    }

    /*
    public void Update()
    {
        GetFriends();
    }*/

    void GetFriends()
    {
        PlayFabClientAPI.GetFriendsList(new GetFriendsListRequest
        {
        }, result =>
        {
            _friends = result.Friends;
            DisplayFriends(_friends); // triggers your UI
        }, DisplayPlayFabError);
    }

    private void DisplayFriends(List<FriendInfo> friends)
    {
        // Limpiar las opciones existentes en el Dropdown
        friendDropdown.ClearOptions();

        // Crear una lista para almacenar las opciones
        List<TMPro.TMP_Dropdown.OptionData> options = new List<TMPro.TMP_Dropdown.OptionData>();

        // Recorrer la lista de amigos y agregar sus nombres como opciones
        foreach (FriendInfo friend in friends)
        {
            // Crear una nueva opción con el nombre del amigo
            TMPro.TMP_Dropdown.OptionData option = new TMPro.TMP_Dropdown.OptionData(friend.Username);

            // Agregar la opción a la lista
            options.Add(option);
        }

        // Establecer las opciones en el Dropdown
        friendDropdown.AddOptions(options);
    }


    void AddFriend(FriendIdType idType)
    {
        string friendId = friendInputField.text; // Obtener el valor del input field

        var request = new AddFriendRequest();

        switch (idType)
        {
            case FriendIdType.PlayFabId:
                request.FriendPlayFabId = friendId;
                break;
            case FriendIdType.Username:
                request.FriendUsername = friendId;
                break;
            case FriendIdType.Email:
                request.FriendEmail = friendId;
                break;
            case FriendIdType.DisplayName:
                request.FriendTitleDisplayName = friendId;
                break;
        }

        // Ejecutar la solicitud y actualizar la lista de amigos cuando haya terminado
        PlayFabClientAPI.AddFriend(request, result =>
        {
            friendsErrors.text = "Friend added successfully!";
            friendsErrors.color = new Color(210, 105, 95, 255);
            // Actualizar la lista de amigos nuevamente llamando a la función que los muestra
            GetFriends();
        }, DisplayPlayFabError);
    }




    // unlike AddFriend, RemoveFriend only takes a PlayFab ID
    // you can get this from the FriendInfo object under FriendPlayFabId
    void RemoveFriend(FriendInfo friendInfo)
    {
        PlayFabClientAPI.RemoveFriend(new RemoveFriendRequest
        {
            FriendPlayFabId = friendInfo.FriendPlayFabId
        }, result =>
        {
            _friends.Remove(friendInfo);
        }, DisplayPlayFabError);
    }


    // this REPLACES the list of tags on the server
    // for updates, make sure this includes the original tag list
    void SetFriendTags(FriendInfo friend, List<string> newTags)
    {
        // update the tags with the edited list
        PlayFabClientAPI.SetFriendTags(new SetFriendTagsRequest
        {
            FriendPlayFabId = friend.FriendPlayFabId,
            Tags = newTags
        }, tagresult =>
        {
            // Make sure to save new tags locally. That way you do not have to hard-update friendlist
            friend.Tags = newTags;
        }, DisplayPlayFabError);
    }


    private void DisplayPlayFabError(PlayFabError obj)
    {
        friendsErrors.text = obj.ErrorMessage;
        friendsErrors.color = Color.red;
    }

}
