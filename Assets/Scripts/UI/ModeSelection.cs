using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModeSelection : MonoBehaviour
{
    public void LoadLocalGameScene()
    {
        SceneManager.LoadScene(SceneNames.LOCAL_GAME);
    }

    public void LoadLANGameScene()
    {
        SceneManager.LoadScene(SceneNames.LAN_LOBBY);
    }

    public void LoadOnlineScene()
    {
        SceneManager.LoadScene(SceneNames.LOGIN);
    }
}
