﻿using System.Collections;
using System.Collections.Generic;
using Complete;
using UnityEngine;
using UnityEngine.UI;


namespace Complete
{
    public class LocalTowerShoots : MonoBehaviour
    {
        public int m_PlayerNumber = 1;              // Used to identify the different players
        public GameObject m_ShellGO;
        public Transform m_FireTransform;           // A child of the tank where the shells are spawned
        public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source
        public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up
        public AudioClip m_FireClip;                // Audio that plays when each shot is fired
        private bool m_reload;
        private float m_timeReload;
        private bool m_enableShoot;

        // Start is called before the first frame update
        void Start()
        {
            m_reload = false;
            m_timeReload = 0f;
            m_enableShoot = false;
        }

        // Update is called once per frame
        void Update()
        {
            m_timeReload += Time.deltaTime;
            if (m_timeReload > 2f)
            {
                m_reload = true;
            }
        }

        public void EnableShoot()
        {
            m_enableShoot = true;
        }

        private void OnTriggerStay(Collider other)
        {
            if (m_reload && other.tag == "Player" && m_enableShoot)
            {
                this.gameObject.GetComponentInChildren<Transform>().transform.LookAt(other.transform.position);
                m_reload = false;
                m_timeReload = 0;
                RptFire();
            }
        }

        private void RptFire()
        {
            // Create an instance of the shell and store a reference to it's rigidbody
            GameObject shellInstance;
            shellInstance = Instantiate(m_ShellGO, m_FireTransform.position, m_FireTransform.rotation);
            shellInstance.GetComponent<LocalTowerShell>().Setup(this.gameObject.transform);
            Physics.IgnoreCollision(shellInstance.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }
}