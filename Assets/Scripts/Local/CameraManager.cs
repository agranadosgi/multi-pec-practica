using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private float SPLIT_VIEWPORT = 0.5f;

    [SerializeField] private Camera backgroundCamera;
    [SerializeField] private Camera generalCamera;
    [SerializeField] private List<Camera> cameras = new List<Camera>();
    [SerializeField] private List<CinemachineVirtualCamera> cameraFollows = new List<CinemachineVirtualCamera>();

    private int currentPlayers;

    public void SetCamerasViewPort(int numPlayers)
    {
        currentPlayers = numPlayers;

        EnableCameras();

        switch (currentPlayers)
        {
            case 2:
                cameras[0].rect = new Rect(0, SPLIT_VIEWPORT, 1, 1);
                cameras[1].rect = new Rect(0, -SPLIT_VIEWPORT, 1, 1);
                break;
            case 3:
                cameras[0].rect = new Rect(-SPLIT_VIEWPORT, SPLIT_VIEWPORT, 1, 1);
                cameras[1].rect = new Rect(SPLIT_VIEWPORT, SPLIT_VIEWPORT, 1, 1);
                cameras[2].rect = new Rect(-SPLIT_VIEWPORT, -SPLIT_VIEWPORT, 1, 1);
                break;
            case 4:
                cameras[0].rect = new Rect(-SPLIT_VIEWPORT, SPLIT_VIEWPORT, 1, 1);
                cameras[1].rect = new Rect(SPLIT_VIEWPORT, SPLIT_VIEWPORT, 1, 1);
                cameras[2].rect = new Rect(-SPLIT_VIEWPORT, -SPLIT_VIEWPORT, 1, 1);
                cameras[3].rect = new Rect(SPLIT_VIEWPORT, -SPLIT_VIEWPORT, 1, 1);
                break;
        }
    }

    public void PairCameraWithPlayer(int playerNum, Transform player)
    {
        cameraFollows[playerNum].Follow = player;
        cameraFollows[playerNum].LookAt = player;
    }

    private void EnableCameras()
    {
        for (int i = 1; i <= cameras.Count; i++)
        {
            cameras[i - 1].enabled = i <= currentPlayers;
        }
        backgroundCamera.enabled = false;
        generalCamera.enabled = currentPlayers == 3;
    }
}
