using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalTankHealth : MonoBehaviour
{
    private LocalGameManager gameManager => LocalGameManager.Instance;

    public float m_StartingHealth = 100f;               // The amount of health each tank starts with.
    public Slider m_Slider;                             // The slider to represent how much health the tank currently has.
    public Image m_FillImage;                           // The image component of the slider.
    public Color m_FullHealthColor = Color.green;       // The color the health bar will be when on full health.
    public Color m_ZeroHealthColor = Color.red;         // The color the health bar will be when on no health.
    public GameObject m_ExplosionPrefab;                // A prefab that will be instantiated in Awake, then used whenever the tank dies.

    private AudioSource m_ExplosionAudio;               // The audio source to play when the tank explodes.
    private ParticleSystem m_ExplosionParticles;        // The particle system the will play when the tank is destroyed.
    private float m_CurrentHealth;                      // How much health the tank currently has.
    private bool m_Dead;                                // Has the tank been reduced beyond zero health yet?

    public Transform lastTank;

    private void Awake()
    {
        // Instantiate the explosion prefab and get a reference to the particle system on it.
        m_ExplosionParticles = Instantiate(m_ExplosionPrefab).GetComponent<ParticleSystem>();

        // Get a reference to the audio source on the instantiated prefab.
        m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource>();

        // Disable the prefab so it can be activated when it's required.
        m_ExplosionParticles.gameObject.SetActive(false);
    }


    private void OnEnable()
    {
        // When the tank is enabled, reset the tank's health and whether or not it's dead.
        m_CurrentHealth = m_StartingHealth;
        m_Dead = false;

        // Update the health slider's value and color.
        SetHealthUI();
    }


    public void TakeDamage(float amount, Transform tank, Color color)
    {
        if (tank.transform == this.transform) return;

        if (this.CompareTag("NPC"))
        {
            m_CurrentHealth -= amount;
            lastTank = tank;
            DestroyTank(1);
        }
        else if (tank.CompareTag("RocketNPC") || tank.CompareTag("NPC"))
        {
            m_CurrentHealth -= amount;
            lastTank = tank;
            DestroyTank(2);
        }
        else if (!gameManager.GetTanksSameColor(tank, this.transform))
        {
            m_CurrentHealth -= amount;
            lastTank = tank;
            DestroyTank(3);
        }

        // Change the UI elements appropriately.
        SetHealthUI();
    }

    public void DestroyTank(int type)
    {
        if (m_CurrentHealth <= 0f)
        {
            //if(lastTank.tag != "RocketNPC" && lastTank.tag != "NPC") {
            if (type == 1) //Just NPCs kills add points to player
            {
                gameManager.SetTankDeaths(lastTank);
            }
            if (gameObject.tag != "NPC")
            {
                OnDeath();
                Respawn();
                RemoveShield();
            }
            else
            {
                OnDeath();
            }
        }
    }

    private void RemoveShield()
    {
        Transform oldShield = gameObject.transform.Find("Shield");
        if (oldShield != null)
        {
            for (int i = 0; i < oldShield.childCount; i++)
            {
                oldShield.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    private void Respawn()
    {
        transform.position = gameManager.GetTankSpawn(transform).position;
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        m_ExplosionParticles.gameObject.SetActive(false);
        m_CurrentHealth = m_StartingHealth;
    }


    private void SetHealthUI()
    {
        // Set the slider's value appropriately.
        m_Slider.value = m_CurrentHealth;

        // Interpolate the color of the bar between the choosen colours based on the current percentage of the starting health.
        m_FillImage.color = Color.Lerp(m_ZeroHealthColor, m_FullHealthColor, m_CurrentHealth / m_StartingHealth);
    }


    private void OnDeath()
    {
        m_ExplosionParticles.transform.position = transform.position;
        m_ExplosionParticles.gameObject.SetActive(true);
        m_ExplosionParticles.Play();
        m_ExplosionAudio.Play();
        if (gameObject.tag == "NPC")
        {
            gameObject.SetActive(false);
        }
    }
}
