using Complete;
using System;
using System.Collections;
using UnityEngine;

public sealed class LocalItemManager : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 0.25f;
    [SerializeField]
    private SphereCollider m_Collider;
    [SerializeField]
    private ItemKind m_ItemKind;
    [SerializeField]
    private GameObject m_ItemSpawn;
    [SerializeField]
    private float m_BuffDuration = 45f;

    private Vector3 m_InitPosition;
    public event Action<LocalItemManager> ItemDestroyed;

    public float SphereRadius => m_Collider.radius;

    private void Start()
    {
        m_InitPosition = transform.position;
    }

    private void Update()
    {
        transform.position = new Vector3(m_InitPosition.x, Mathf.PingPong(Time.time * m_Speed, 0.25f) + m_InitPosition.y + 0.5f, m_InitPosition.z);
        transform.Rotate(new Vector3(0f, m_Speed, 0f));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ItemPickUp(other.transform);
        }
    }

    void ItemPickUp(Transform instigator)
    {
        if (GameManager.GameFinished)
        {
            return;
        }
        switch (m_ItemKind)
        {
            case ItemKind.Shield:
                Transform oldShield = instigator.transform.Find("Shield");
                if (oldShield != null)
                {
                    for (int i = 0; i < oldShield.childCount; i++)
                    {
                        oldShield.GetChild(i).gameObject.SetActive(true);
                    }
                }
                break;
            case ItemKind.AltFire:
                LocalTankShooting tankShooting = instigator.GetComponent<LocalTankShooting>();
                tankShooting.StartCoroutine(AltFireDuration(tankShooting));
                break;
            case ItemKind.Laser:
                tankShooting = instigator.GetComponent<LocalTankShooting>();
                tankShooting.StartCoroutine(LaserDuration(tankShooting));
                break;
            default:
                break;
        }
        Destroy(gameObject);
    }

    private IEnumerator AltFireDuration(LocalTankShooting tankShooting)
    {
        tankShooting.HasAltFire = true;
        yield return new WaitForSeconds(m_BuffDuration);
        tankShooting.HasAltFire = false;
    }

    private IEnumerator LaserDuration(LocalTankShooting tankShooting)
    {
        tankShooting.HasLaser = true;
        yield return new WaitForSeconds(m_BuffDuration);
        tankShooting.HasLaser = false;
    }

    public enum ItemKind
    {
        Shield,
        AltFire,
        Laser
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(m_InitPosition, SphereRadius);
    }

    private void OnDestroy()
    {
        ItemDestroyed?.Invoke(this);
    }
}
