using Complete;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LocalGameManager : MonoBehaviour
{
    public static LocalGameManager Instance;

    public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game.
    public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases.
    public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases.
    public CameraManager m_CameraManager;       // Reference to the CameraControl script for control during different phases.
    public GameObject m_NumPlayerCanvas;        // Reference to the Canvas containing the num player buttons.
    public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
    public GameObject m_TankPrefab;             // Reference to the prefab the players will control.
    public List<LocalTankManager> m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks.
    public List<GameObject> npc_Tanks;
    public int npcEnemyCount;
    private bool spawnOccupied = false;
    public GameObject[] towerObjects;
    public GameObject enemyPrefab;
    public GameObject bossPrefab;
    private float timeToSpawnBoss;
    public List<GameObject> rocket_Tanks;
    public PlayerInputManager m_InputManager;   // Reference to the PlayerInputManager
    public GameObject m_PlayerInput;            // Reference to GameObject waiting for new players
    public GameObject[] m_JoinPanel;            // Reference to JoinPanel
    private int m_RoundNumber;                  // Which round the game is currently on.
    private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts.
    private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends.
    private Color m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won.
    private LocalTankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won.

    private int m_NumPlayers = 0;
    private string[] schemes = { "WASD", "Arrows", "IJKL", "GVBN" };
    private PlayerInput[] m_Players = new PlayerInput[4];

    [SerializeField] private GameObject controlsPanel;

    [Header("Game UI")]
    [SerializeField] private GameObject playerUI;
    public TMP_Text blueTeamScoreText;
    public TMP_Text redTeamScoreText;
    public TMP_Text blueRoundText;
    public TMP_Text redRoundText;

    private int blueTeamScore = 0;
    private int redTeamScore = 0;
    private int blueRoundScore = 0;
    private int redRoundScore = 0;

    private bool hasGameStarted = false;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (hasGameStarted)
        {
            timeToSpawnBoss += Time.deltaTime;
        }
        if (timeToSpawnBoss > 20f)
        {
            timeToSpawnBoss = 0;
            CreateBoss();
        }
    }

    public void StartGameWithPlayers(int numPlayers)
    {
        m_NumPlayers = numPlayers;

        InitializeGame();

        m_NumPlayerCanvas.SetActive(false);
        playerUI.SetActive(true);
    }

    private void InitializeGame()
    {
        // Create the delays so they only have to be made once.
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        SpawnInitialPlayerTanks();
        SetCameraProperties();

        CreateNPCs();
        EnableTowers();

        // Once the tanks have been created and the camera is using them as targets, start the game.
        StartCoroutine(GameLoop());
    }

    private void SpawnInitialPlayerTanks()
    {
        // For all the tanks...
        for (int i = 0; i < m_InputManager.maxPlayerCount; i++)
        {
            PlayerInput input = PlayerInput.Instantiate(m_PlayerInput, i, schemes[i], -1, Keyboard.current);
            m_Players[i] = input;

            if (i < m_NumPlayers)
            {
                input.SwitchCurrentActionMap("Tank");
                GameObject tank = Instantiate(m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation);
                m_Tanks[i].m_Instance = tank;
                m_Tanks[i].m_PlayerColor = i % 2 == 0 ? Color.red : Color.blue;
                tank.GetComponent<LocalTankMovement>().m_TankInput = input;
                LocalTankShooting shooting = tank.GetComponent<LocalTankShooting>();
                shooting.m_TankInput = input;
                shooting.color = m_Tanks[i].m_PlayerColor;

                m_CameraManager.PairCameraWithPlayer(i, m_Tanks[i].m_Instance.transform);

                m_Tanks[i].m_PlayerNumber = i + 1;
                m_Tanks[i].Setup();
            }
            else
            {
                ManageJoinPanels(input.currentControlScheme, true);
                input.actions["Join"].performed += ctx => JoinNewTank(input.playerIndex);
            }
        }
    }

    private void JoinNewTank(int playerIndex)
    {
        Debug.Log(playerIndex);
        m_Players[playerIndex].actions["Join"].performed -= ctx => { };

        ManageJoinPanels(m_Players[playerIndex].currentControlScheme, false);

        m_Players[playerIndex].SwitchCurrentActionMap("Player");
        GameObject tank = Instantiate(m_TankPrefab, m_Tanks[m_NumPlayers].m_SpawnPoint.position, m_Tanks[m_NumPlayers].m_SpawnPoint.rotation);
        m_Tanks[m_NumPlayers].m_Instance = tank;
        tank.GetComponent<LocalTankMovement>().m_TankInput = m_Players[playerIndex];
        tank.GetComponent<LocalTankShooting>().m_TankInput = m_Players[playerIndex];

        m_CameraManager.PairCameraWithPlayer(m_NumPlayers, m_Tanks[m_NumPlayers].m_Instance.transform);

        m_Tanks[m_NumPlayers].m_PlayerNumber = m_NumPlayers + 1;
        m_Tanks[m_NumPlayers].Setup();

        m_NumPlayers++;

        m_CameraManager.SetCamerasViewPort(m_NumPlayers);
    }

    private void SetCameraProperties()
    {
        // Set the view port of the cameras depending on the number of players
        m_CameraManager.SetCamerasViewPort(m_NumPlayers);
    }

    private void ManageJoinPanels(string controlScheme, bool active)
    {
        if (controlScheme.Equals("IJKL"))
        {
            m_JoinPanel[0].SetActive(active);
        }
        else
        {
            m_JoinPanel[1].SetActive(active);
        }
    }

    public bool GetTanksSameColor(Transform player1, Transform player2)
    {
        LocalTankManager manager1 = m_Tanks.Find(tank => tank.m_Instance.transform == player1);
        LocalTankManager manager2 = m_Tanks.Find(tank => tank.m_Instance.transform == player2);

        return manager1.m_PlayerColor == manager2.m_PlayerColor;
    }

    public void SetTankDeaths(Transform player)
    {
        LocalTankManager manager = m_Tanks.Find(tank => tank.m_Instance.transform == player);

        if (manager != null)
        {
            manager.m_RoundKills++;

            UpdateTeamScore(manager.m_PlayerColor);
        }
    }

    private void UpdateTeamScore(Color teamColor)
    {
        if (teamColor == Color.red)
        {
            redTeamScore++;
        }
        else if (teamColor == Color.blue)
        {
            blueTeamScore++;
        }
        redTeamScoreText.SetText("Points: " + redTeamScore.ToString());
        blueTeamScoreText.SetText("Points: " + blueTeamScore.ToString());
    }

    private void UpdateTeamRound(Color teamColor)
    {

        if (teamColor == Color.red)
        {
            redRoundScore++;
            redRoundText.SetText(redRoundScore.ToString());
        }
        else if (teamColor == Color.blue)
        {
            blueRoundScore++;
            blueRoundText.SetText(blueRoundScore.ToString());
        }
    }

    // This is called from start and will run each phase of the game one after another.
    private IEnumerator GameLoop()
    {
        // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundStarting());

        // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundPlaying());

        // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
        yield return StartCoroutine(RoundEnding());

        // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found.
        if (m_GameWinner != null)
        {
            // If there is a game winner, restart the level.
            SceneManager.LoadScene(SceneNames.LOCAL_GAME);
        }
        else
        {
            ResetRoundKills();
            CreateNPCs();
            // If there isn't a winner yet, restart this coroutine so the loop continues.
            // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end.
            StartCoroutine(GameLoop());
        }
    }


    private IEnumerator RoundStarting()
    {
        // As soon as the round starts reset the tanks and make sure they can't move.
        hasGameStarted = true;
        ResetAllTanks();
        DisableTankControl();

        redTeamScore = 0;
        blueTeamScore = 0;
        UpdateTeamScore(Color.clear);

        // Snap the camera's zoom and position to something appropriate for the reset tanks.
        //m_CameraControl.SetStartPositionAndSize ();

        // Increment the round number and display text showing the players what round it is.
        m_RoundNumber++;
        m_MessageText.text = "ROUND " + m_RoundNumber;

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
        // As soon as the round begins playing let the players control the tanks.
        EnableTankControl();

        // Clear the text from the screen.
        m_MessageText.text = string.Empty;

        // While there is not one tank left...
        while (!NoNPCsLeft())
        {
            // ... return on the next frame.
            yield return null;
        }
    }


    private IEnumerator RoundEnding()
    {
        // Stop tanks from moving.
        hasGameStarted = false;
        DisableTankControl();
        DeleteNPCs();

        // Clear the winner from the previous round.
        m_RoundWinner = Color.black;

        // See if there is a winner now the round is over.
        m_RoundWinner = GetRoundWinner();

        UpdateTeamRound(m_RoundWinner);

        // If there is a winner, increment their score.
        if (m_RoundWinner != Color.black)
        {
            foreach (LocalTankManager tankInstance in m_Tanks)
            {
                if (tankInstance.m_PlayerColor == m_RoundWinner)
                    tankInstance.m_Wins++;
                Debug.Log("Victorias: " + tankInstance.m_Wins);
            }
        }

        // Now the winner's score has been incremented, see if someone has one the game.
        m_GameWinner = GetGameWinner();

        // Get a message based on the scores and whether or not there is a game winner and display it.
        string message = EndMessage();
        m_MessageText.text = message;

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_EndWait;
    }


    // This is used to check if there is one or fewer tanks remaining and thus the round should end.
    private bool NoNPCsLeft()
    {
        // Start the count of tanks left at zero.
        int npcsLeft = 0;

        // Go through all the tanks...
        for (int i = 0; i < npc_Tanks.Count; i++)
        {
            // ... and if they are active, increment the counter.
            if (npc_Tanks[i].activeSelf)
                npcsLeft++;
        }

        // If there are one or fewer tanks remaining return true, otherwise return false.
        return npcsLeft == 0;
    }

    private void DeleteNPCs()
    {
        for (int i = 0; i < npc_Tanks.Count; i++)
        {
            Destroy(npc_Tanks[i]);
        }
        npc_Tanks.Clear();
    }

    private void ResetRoundKills()
    {
        m_Tanks.ForEach(tank => tank.m_RoundKills = 0);
    }


    // This function is to find out if there is a winner of the round.
    // This function is called with the assumption that 1 or fewer tanks are currently active.
    private Color GetRoundWinner()
    {
        int blueTeamPoints = 0;
        int redTeamPoints = 0;
        Color winner = Color.black;
        foreach (LocalTankManager tankInstance in m_Tanks)
        {
            if (tankInstance.m_PlayerColor == Color.blue)
                blueTeamPoints += tankInstance.m_RoundKills;
            if (tankInstance.m_PlayerColor == Color.red)
                redTeamPoints += tankInstance.m_RoundKills;
        }
        if (blueTeamPoints > redTeamPoints)
        {
            winner = Color.blue;
        }
        else if (blueTeamPoints < redTeamPoints)
        {
            winner = Color.red;
        }
        return winner;
    }


    // This function is to find out if there is a winner of the game.
    private LocalTankManager GetGameWinner()
    {
        // Go through all the tanks...
        for (int i = 0; i < m_NumPlayers; i++)
        {
            // ... and if one of them has enough rounds to win the game, return it.
            if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                return m_Tanks[i];
        }

        // If no tanks have enough rounds to win, return null.
        return null;
    }


    // Returns a string message to display at the end of each round.
    private string EndMessage()
    {
        // By default when a round ends there are no winners so the default end message is a draw.
        string message = "DRAW!";

        // If there is a winner then change the message to reflect that.
        if (m_RoundWinner == Color.blue)
            message = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.blue) + ">" + "TEAM BLUE WINS THE ROUND" + "</color>";
        else if (m_RoundWinner == Color.red)
            message = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.red) + ">" + "TEAM RED WINS THE ROUND" + "</color>";

        // Add some line breaks after the initial message.
        message += "\n\n";

        // Go through all the tanks and add each of their scores to the message.
        for (int i = 0; i < m_Tanks.Count; i++)
        {
            message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
        }

        // If there is a game winner, change the entire message to reflect that.
        if (m_GameWinner != null)
        {
            if (m_GameWinner.m_PlayerColor == Color.blue)
            {
                message = "<color=#" + ColorUtility.ToHtmlStringRGB(m_GameWinner.m_PlayerColor) + ">" + "TEAM BLUE WINS THE GAME" + "</color>";
            }
            else if (m_GameWinner.m_PlayerColor == Color.red)
            {
                message = "<color=#" + ColorUtility.ToHtmlStringRGB(m_GameWinner.m_PlayerColor) + ">" + "TEAM RED WINS THE GAME" + "</color>";
            }
        }

        return message;
    }

    private void CreateNPCs()
    {
        for (int i = 0; i < npcEnemyCount; i++)
        {
            var spawnPos = RandomPosition();
            var spawnRot = RandomQuaternion();
            spawnOccupied = true;

            while (spawnOccupied)
            {
                var hitColliders = Physics.OverlapSphere(spawnPos, 2);
                if (hitColliders.Length > 1) // If hit with more than ground, get another random position
                    spawnPos = RandomPosition();
                else
                {
                    GameObject go = Instantiate(enemyPrefab, spawnPos, spawnRot);
                    npc_Tanks.Add(go);
                    spawnOccupied = false;
                }
            }
        }
    }

    private void CreateBoss()
    {
        var spawnPos = RandomPosition();
        var spawnRot = RandomQuaternion();
        spawnOccupied = true;

        while (spawnOccupied)
        {
            var hitColliders = Physics.OverlapSphere(spawnPos, 2);
            if (hitColliders.Length > 1) // If hit with more than ground, get another random position
                spawnPos = RandomPosition();
            else
            {
                Instantiate(bossPrefab, spawnPos, spawnRot);
                spawnOccupied = false;
            }
        }
    }

    private void EnableTowers()
    {
        foreach (GameObject tower in towerObjects)
        {
            tower.GetComponent<LocalTowerShoots>().EnableShoot();
        }
    }

    private Vector3 RandomPosition()
    {
        return new Vector3(Random.Range(-30.0f, 30.0f), 0.0f, Random.Range(-30.0f, 30.0f));
    }

    private Quaternion RandomQuaternion()
    {
        return Quaternion.Euler(0.0f, Random.Range(0, 360), 0.0f);
    }

    // This function is used to turn all the tanks back on and reset their positions and properties.
    private void ResetAllTanks()
    {
        for (int i = 0; i < m_NumPlayers; i++)
        {
            m_Tanks[i].Reset();
        }
    }

    private void EnableTankControl()
    {
        for (int i = 0; i < m_NumPlayers; i++)
        {
            m_Tanks[i].EnableControl();
        }
    }


    private void DisableTankControl()
    {
        for (int i = 0; i < m_NumPlayers; i++)
        {
            m_Tanks[i].DisableControl();
        }
    }

    public Transform GetTankSpawn(Transform tank)
    {
        return m_Tanks.Find(tankManager => tankManager.m_Instance.transform == tank).m_SpawnPoint;
    }

    public void ShowControlsPanel()
    {
        controlsPanel.SetActive(true);
    }

    public void DimissControlsPanel()
    {
        controlsPanel.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
