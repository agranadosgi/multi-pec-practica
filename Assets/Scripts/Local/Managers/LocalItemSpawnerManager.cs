using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public sealed class LocalItemSpawnerManager : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Area;
    [SerializeField]
    private float m_SpawnDelay = 3f;
    [SerializeField]
    private int m_MaxItemsSpawned = 20;
    [SerializeField]
    private LocalItemManager[] m_Items;
    [SerializeField]
    private LayerMask m_GroundLayer;

    private float m_EllapsedTime;
    private List<LocalItemManager> m_SpawnedItems;

    private void Start()
    {
        m_SpawnedItems = new List<LocalItemManager>();
    }

    private void Update()
    {
        if (m_SpawnedItems.Count < m_MaxItemsSpawned)
        {
            m_EllapsedTime += Time.deltaTime;
            if (m_EllapsedTime > m_SpawnDelay)
            {
                StartCoroutine(SpawnItem());
                m_EllapsedTime = 0f;
            }
        }
    }

    private Vector3 GetRandomPosition()
    {
        if (m_Area.TryGetComponent(out Collider collider))
        {
            return new Vector3(Random.Range(m_Area.transform.position.x - (m_Area.transform.localScale.x * collider.bounds.size.x * 0.5f),
                                                        m_Area.transform.position.x + (m_Area.transform.localScale.x * collider.bounds.size.x * 0.5f)),
                               m_Area.transform.position.y,
                               Random.Range(m_Area.transform.position.z - (m_Area.transform.localScale.z * collider.bounds.size.z * 0.5f),
                                                        m_Area.transform.position.z + (m_Area.transform.localScale.z * collider.bounds.size.z * 0.5f)));
        }
        return gameObject.transform.position + new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
    }

    private IEnumerator SpawnItem()
    {
        Vector3 randomPosition;
        int randomItem;
        do
        {
            randomPosition = GetRandomPosition();
            randomItem = Random.Range(0, m_Items.Length);
            yield return null;
        } while (IsInvalidPosition(randomPosition, m_Items[randomItem].SphereRadius));

        ServerSpawnItem(randomPosition, randomItem);
    }

    private void ServerSpawnItem(Vector3 randomPosition, int randomItem)
    {
        LocalItemManager item = Instantiate(m_Items[randomItem], randomPosition, Quaternion.identity);
        item.ItemDestroyed += OnItemDestroyed;
        m_SpawnedItems.Add(item);
    }

    private void OnItemDestroyed(LocalItemManager obj)
    {
        obj.ItemDestroyed -= OnItemDestroyed;
        m_SpawnedItems.Remove(obj);
    }

    private bool IsInvalidPosition(Vector3 pos, float radius)
    {
        RaycastHit[] hits = Physics.SphereCastAll(pos, radius, pos, radius, ~m_GroundLayer); //~
        DrawSphere(pos, radius, Color.yellow);
        return hits.Any(h => h.collider.gameObject.isStatic);
    }

    public static void DrawSphere(Vector4 pos, float radius, Color color)
    {
        Vector4[] v = s_UnitSphere;
        int len = s_UnitSphere.Length / 3;
        for (int i = 0; i < len; i++)
        {
            Vector4 sX = pos + (radius * v[(0 * len) + i]);
            Vector4 eX = pos + (radius * v[(0 * len) + ((i + 1) % len)]);
            Vector4 sY = pos + (radius * v[(1 * len) + i]);
            Vector4 eY = pos + (radius * v[(1 * len) + ((i + 1) % len)]);
            Vector4 sZ = pos + (radius * v[(2 * len) + i]);
            Vector4 eZ = pos + (radius * v[(2 * len) + ((i + 1) % len)]);
            Debug.DrawLine(sX, eX, color);
            Debug.DrawLine(sY, eY, color);
            Debug.DrawLine(sZ, eZ, color);
        }
    }
    private static readonly Vector4[] s_UnitSphere = MakeUnitSphere(16);
    private static Vector4[] MakeUnitSphere(int len)
    {
        Debug.Assert(len > 2);
        Vector4[] v = new Vector4[len * 3];
        for (int i = 0; i < len; i++)
        {
            float f = i / (float)len;
            float c = Mathf.Cos(f * (float)(Mathf.PI * 2.0));
            float s = Mathf.Sin(f * (float)(Mathf.PI * 2.0));
            v[(0 * len) + i] = new Vector4(c, s, 0, 1);
            v[(1 * len) + i] = new Vector4(0, c, s, 1);
            v[(2 * len) + i] = new Vector4(s, 0, c, 1);
        }
        return v;
    }
}
