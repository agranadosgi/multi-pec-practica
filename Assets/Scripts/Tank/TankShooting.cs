﻿using Mirror;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Complete
{
    public class TankShooting : NetworkBehaviour
    {
        public int m_PlayerNumber = 1;              // Used to identify the different players
        public GameObject m_Shell;                  // Prefab of the shell
        public GameObject m_ShellAlt;               // Prefab of the shell alt
        public GameObject m_ShellLaser;             // Prefab of the shell laser
        public Transform m_FireTransform;           // A child of the tank where the shells are spawned
        public Transform m_LaserTransform;          // A child of the tank where the lasers are spawned
        public Slider m_AimSlider;                  // A child of the tank that displays the current launch force
        public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source
        public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up
        public AudioClip m_FireClip;                // Audio that plays when each shot is fired
        public float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held
        public float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time
        public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force
        private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released
        private InputAction m_FireAction;           // Fire Action reference (Unity 2020 New Input System)
        private InputAction m_FireAltAction;        // Fire Alt Action reference (Unity 2020 New Input System)
        private InputAction m_FireLaserAction;      // Fire Alt Action reference (Unity 2020 New Input System)
        private bool isDisabled = false;            // To avoid enabling / disabling Input System when tank is destroyed
        private bool isLaserReady = true;
        private float timeToReload;

        [SyncVar(hook = nameof(OnChangeColor))]
        private Color color;

        public bool HasLaser { get; set; } = false;
        public bool HasAltFire { get; set; } = false;

        private void OnChangeColor(Color oldValue, Color newValue)
        {
            color = newValue;
        }


        private void OnEnable()
        {
            // When the tank is turned on, reset the launch force and the UI
            m_CurrentLaunchForce = m_MinLaunchForce;
            m_AimSlider.value = m_MinLaunchForce;
            isDisabled = false;
        }

        private void OnDisable()
        {
            isDisabled = true;
        }

        private void Update()
        {
            if (isLocalPlayer)
            {
                // color = GetComponent<TankManager>().m_PlayerColor;
                timeToReload += Time.deltaTime;
                if (timeToReload > 5f)
                {
                    isLaserReady = true;
                }
            }
        }

        public void SetColor(Color newColor)
        {
            OnChangeColor(Color.grey, newColor);
        }

        public Color GetColor()
        {
            return color;
        }

        private void Start()
        {
            // Unity 2020 New Input System
            // Get a reference to the EventSystem for this player
            EventSystem ev = GameObject.Find("EventSystem").GetComponent<EventSystem>();

            // Find the Action Map for the Tank actions and enable it
            InputActionMap playerActionMap = ev.GetComponent<PlayerInput>().actions.FindActionMap("Tank");
            playerActionMap.Enable();

            // Find the 'Fire' action
            m_FireAction = playerActionMap.FindAction("Fire");
            m_FireAltAction = playerActionMap.FindAction("FireAlt");
            m_FireLaserAction = playerActionMap.FindAction("Laser");

            m_FireAction.Enable();
            m_FireAltAction.Enable();
            m_FireLaserAction.Enable();
            m_FireAction.performed += OnFire;
            m_FireAltAction.performed += OnFireAlt;
            m_FireLaserAction.performed += OnLaser;
            timeToReload = 0f;
            isLaserReady = true;
        }

        // Event called when this player's 'Fire' action is triggered by the New Input System
        public void OnFire(InputAction.CallbackContext obj)
        {
            if (!isOwned)
            {
                return;
            }
            if (!isDisabled)
            {
                // When the value read is higher than the default Button Press Point, the key has been pressed
                if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
                {
                    Fire(0);
                }
            }
        }

        // Event called when this player's 'Fire' action is triggered by the New Input System
        public void OnFireAlt(InputAction.CallbackContext obj)
        {
            if (!isOwned)
            {
                return;
            }
            if (!HasAltFire)
            {
                return;
            }
            if (!isDisabled)
            {
                // When the value read is higher than the default Button Press Point, the key has been pressed
                if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
                {
                    Fire(1);
                }
            }
        }

        // Event called when this player's 'Fire' action is triggered by the New Input System
        public void OnLaser(InputAction.CallbackContext obj)
        {
            if (!isOwned)
            {
                return;
            }
            if (!HasLaser)
            {
                return;
            }
            if (!isDisabled && isLaserReady)
            {
                // When the value read is higher than the default Button Press Point, the key has been pressed
                if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
                {
                    isLaserReady = false;
                    timeToReload = 0;
                    Fire(2);
                }
            }
        }

        private void Fire(int shootType)
        {
            CmdFire(shootType);
            // Change the clip to the firing clip and play it
            m_ShootingAudio.clip = m_FireClip;
            m_ShootingAudio.Play();

            // Reset the launch force.  This is a precaution in case of missing button events
            m_CurrentLaunchForce = m_MinLaunchForce;
        }

        [Command]
        void CmdFire(int shootType)
        {
            GameObject bullet;
            if (shootType == 0)
            {
                //Regular Shell
                bullet = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation);
                bullet.GetComponent<ShellExplosion>().Setup(m_MinLaunchForce, transform, color);
            }
            else if (shootType == 1)
            {
                //Alt shell
                bullet = Instantiate(m_ShellAlt, m_FireTransform.position, m_FireTransform.rotation);
                bullet.GetComponent<ShellExplosion>().Setup(m_MinLaunchForce * 1.50f, transform, color);
            }
            else
            {
                //Laser Beam
                bullet = Instantiate(m_ShellLaser, m_LaserTransform.position, m_LaserTransform.rotation);
                bullet.GetComponent<ShellExplosion>().Setup(m_MinLaunchForce * 3f, transform, color);
            }

            Transform shield = transform.Find("Shield");
            ServerIgnoreShieldCollisions(bullet.transform, shield);
            NetworkServer.Spawn(bullet);

        }

        [Server]
        private void ServerIgnoreShieldCollisions(Transform bullet, Transform shield)
        {
            if (shield != null)
            {
                foreach (Collider collider in shield.GetComponentsInChildren<Collider>())
                {
                    Physics.IgnoreCollision(bullet.GetComponent<Collider>(), collider, true);
                }
            }
        }
    }
}