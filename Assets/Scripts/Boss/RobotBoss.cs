﻿using UnityEngine;
using Mirror;
using Complete;

public class RobotBoss : NetworkBehaviour
{
    public float moveSpeed = 5f;
    public float rotationSpeed = 10f;
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;
    private GameObject targetPlayer;
    private bool m_reload;
    private float m_timeReload;
    private float m_timetoChangeTarget;
    private float minimumDistance = 6f;

    private void Start()
    {
        m_reload = false;
        m_timeReload = 0f;
        m_timetoChangeTarget = 0f;
        GetTarget();
    }


    private void GetTarget()
    {
        // Find random player
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 0)
        {
            int randomIndex = Random.Range(0, players.Length);
            targetPlayer = players[randomIndex];
        }
    }

    private void Update()
    {
        if (!isServer)
            return;

        //Time to reload
        m_timeReload += Time.deltaTime;
        if (m_timeReload > 3f)
        {
            m_timeReload = 0f;
            m_reload = true;
        }

        //Change target
        m_timetoChangeTarget += Time.deltaTime;
        if (m_timetoChangeTarget > 5f)
        {
            m_timetoChangeTarget = 0f;
            GetTarget();
        }


        if (targetPlayer != null)
        {
            // Move to target
            Vector3 targetDirection = targetPlayer.transform.position - transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            if (targetDirection.magnitude > minimumDistance)
            {
                transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
            }
            // shoot to target
            if (m_reload) {
                m_reload = false;
                RptFireBullet();
            }
        }
    }

    //[Command]
    //private void CmdFireBullet()
    [ClientRpc]
    private void RptFireBullet()
    {
        // Create bullet
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        bullet.GetComponent<TowerShell>().Setup(this.gameObject.transform);
        // Asignar el jugador objetivo al componente de la bala para que pueda seguirlo
        //        Bullet bulletScript = bullet.GetComponent<Bullet>();
        //        bulletScript.SetTarget(targetPlayer);

        // Spawneamos la bala en los clientes
        //        NetworkServer.Spawn(bullet);
    }
}
