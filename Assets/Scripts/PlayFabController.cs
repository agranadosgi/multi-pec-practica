using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayFabController : MonoBehaviour
{
    public static PlayFabController Instance;

    public static event Action OnShowLogin;
    public static event Action OnShowDisplayNameMenu;
    public static event Action<bool> OnDisplayNameUpdated;
    public static event Action<string> OnError;

    public string displayName { get; private set; }

    private string email;
    private string password;


    private bool firstLogin = true;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void Awake()
    {
        CreateSingleton();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == SceneNames.LOGIN)
        {
            ResetPlayerValues();
            CheckUserForLogin();
        }
    }

    private void CheckUserForLogin()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
        {
            PlayFabSettings.staticSettings.TitleId = "5BE9D";
        }

        if (PlayerPrefs.HasKey("EMAIL"))
        {
            LoginUser(PlayerPrefs.GetString("EMAIL"), PlayerPrefs.GetString("PASSWORD"));
        }
        else
        {
            ShowLoginMenu();
            firstLogin = false;
        }
    }

    #region Login
    public void LoginUser(string email, string password)
    {
        this.email = email;
        this.password = password;
        var request = new LoginWithEmailAddressRequest
        {
            Email = email,
            Password = password,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true
            }
        };
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        if (firstLogin) firstLogin = false;

        PlayerPrefs.SetString("EMAIL", email);
        PlayerPrefs.SetString("PASSWORD", password);

        string displayName = null;
        if (result.InfoResultPayload.PlayerProfile != null)
        {
            displayName = result.InfoResultPayload.PlayerProfile.DisplayName;
        }

        if (displayName != null)
        {
            this.displayName = displayName;
            SceneManager.LoadScene(SceneNames.ONLINE_LOBBY);
        }
        else
        {
            ShowPlayerDisplayMenu();
        }
    }

    private void OnLoginFailure(PlayFabError error)
    {
        email = "";
        password = "";
        ShowLoginMenu();

        if (!firstLogin)
        {
            ShowErrorMessage(error);
        }

        if (firstLogin) firstLogin = false;
    }
    #endregion

    #region Register
    public void RegisterUser(string email, string password, string username)
    {
        this.email = email;
        this.password = password;

        var registerRequest = new RegisterPlayFabUserRequest
        {
            Email = email,
            Password = password,
            Username = username,
        };
        PlayFabClientAPI.RegisterPlayFabUser(registerRequest, OnRegisterSuccess, OnRegisterFailure);
    }

    private void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        PlayerPrefs.SetString("EMAIL", email);
        PlayerPrefs.SetString("PASSWORD", password);

        ShowPlayerDisplayMenu();
    }

    private void OnRegisterFailure(PlayFabError error)
    {
        email = "";
        password = "";
        ShowErrorMessage(error);
    }
    #endregion

    #region Account Info
    public void SetUserDisplayName(string displayName)
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = displayName
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnUpdateDisplayNameSuccess, OnDisplayNameFailure);
    }

    private void OnUpdateDisplayNameSuccess(UpdateUserTitleDisplayNameResult result)
    {
        displayName = result.DisplayName;
        Debug.Log("Player display name: " + displayName);

        if (SceneManager.GetActiveScene().name == SceneNames.LOGIN)
        {
            SceneManager.LoadScene(SceneNames.ONLINE_LOBBY);
        }
        else
        {
            OnDisplayNameUpdated?.Invoke(true);
        }
    }

    private void OnDisplayNameFailure(PlayFabError error)
    {
        OnDisplayNameUpdated?.Invoke(false);
        ShowErrorMessage(error);
    }
    #endregion

    private void ShowLoginMenu()
    {
        OnShowLogin?.Invoke();
    }

    private void ShowErrorMessage(PlayFabError error)
    {
        OnError?.Invoke(error.ErrorMessage);
    }

    private void ShowPlayerDisplayMenu()
    {
        OnShowDisplayNameMenu?.Invoke();
    }

    private void ResetPlayerValues()
    {
        displayName = null;
        email = null;
        password = null;
    }

    public void LogOut()
    {
        PlayerPrefs.DeleteAll();
        PlayFabClientAPI.ForgetAllCredentials();
        SceneManager.LoadScene(SceneNames.LOGIN);
    }


    //MODIFICAAR
    public void SubmitScore(int playerScore, string playerName)
    {
        Debug.Log("Display Name:" + displayName + " Player Name:" + playerName);
        if (displayName == playerName) { 
            PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate> {
                        new StatisticUpdate {
                            StatisticName = "Tanks Leaderboard",
                            Value = playerScore
                        }
                    }
            }, result => OnStatisticsUpdated(result), FailureCallback);
        }
    }

    private void OnStatisticsUpdated(UpdatePlayerStatisticsResult updateResult)
    {
        Debug.Log("Successfully submitted high score");
    }

    private void FailureCallback(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your API call. Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }


    #region Singleton
    private void CreateSingleton()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    #endregion
}
