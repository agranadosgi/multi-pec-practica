using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Complete
{
    public class GameManager : NetworkBehaviour
    {
        public static GameManager Instance;

        public static Action OnPlayerStopGame;
        public static Action OnServerStopGame;

        public int m_NumRoundsToWin = 3;            // The number of rounds a single player has to win to win the game
        public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases
        public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases
        public CameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases
        public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc
        public Text m_MessageWaitingText;           // Reference to the overlay Text to display waiting players
        public GameObject m_TankPrefab;             // Reference to the prefab the players will control
        public List<TankManager> m_Tanks;           // A collection of managers for enabling and disabling different aspects of the tanks
        public List<GameObject> npc_Tanks;          // A collection of managers for enabling and disabling different aspects of the tanks
        public List<GameObject> rocket_Tanks;       // A collection of managers for enabling and disabling different aspects of the tanks
        public readonly SyncList<Transform> cameraTargets = new SyncList<Transform>();
        public GameObject enemyPrefab;
        public int npcEnemyCount;
        private bool spawnOccupied = false;
        public int actualPlayerOnGame = 0;
        private int m_RoundNumber;                  // Which round the game is currently on
        private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts
        private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends
        private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won
        private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won
        private Color m_TeamRoundWinner;            // Color of the team has won the game
        public bool teamMatch;                      //TRUE = Team match. FALSE = Solo match.
        public GameObject bossPrefab;
        private float timeToSpawnBoss;
        private bool hasGameStarted = false;
        private GameObject[] spawnObjects;
        private int maxPoint;
        private int currentPoint;
        private GameObject[] towerObjects;
        public TMPro.TextMeshProUGUI redTeam;
        public TMPro.TextMeshProUGUI blueTeam;

        private int blueTeamScore = 0;
        private int redTeamScore = 0;
        private int blueRoundScore = 0;
        private int redRoundScore = 0;

        public TMP_Text blueTeamScoreText;
        public TMP_Text redTeamScoreText;
        public TMP_Text blueRoundText;
        public TMP_Text redRoundText;

        public static event Action OnStartGame;

        public static bool GameFinished {get; private set;}

        void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            cameraTargets.Callback += OnTargetCreated;
            teamMatch = true;
            spawnObjects = GameObject.FindGameObjectsWithTag("Spawn");
            maxPoint = spawnObjects.Length;
            currentPoint = 0;
        }

        private void Update()
        {
            if (isServer)
            {
                RemovePlayerFromCam();
                if (hasGameStarted)
                {
                    timeToSpawnBoss += Time.deltaTime;
                }
                if (timeToSpawnBoss > 20f)
                {
                    timeToSpawnBoss = 0;
                    CreateBoss();
                }
            }
        }

        public int GetRespawnPoint()
        {
            if (currentPoint == maxPoint - 1)
            {
                currentPoint = 0;
            }
            else
            {
                currentPoint++;
            }
            return currentPoint;
        }

        private void EnableTowers()
        {
            towerObjects = GameObject.FindGameObjectsWithTag("RocketNPC");
            foreach (GameObject tower in towerObjects)
            {
                tower.GetComponent<TowerShoots>().EnableShoot();
            }
        }

        [Server]
        public void StartGame()
        {
            if (!isServer) return;

            // Create the delays so they only have to be made once
            m_StartWait = new WaitForSeconds(m_StartDelay);
            m_EndWait = new WaitForSeconds(m_EndDelay);

            CreateNPCs();
            AddPlayersToCamList();
            AddNPCsToCamList();
            EnableTowers();
            // Once the tanks have been created and the camera is using them as targets, start the game
            if (!hasGameStarted)
            {
                hasGameStarted = true;
                StartCoroutine(GameLoop());
            }
        }

        private void OnTargetCreated(SyncList<Transform>.Operation op, int index, Transform oldItem, Transform newItem)
        {
            switch (op)
            {
                case SyncList<Transform>.Operation.OP_ADD:
                    SetCameraTarget(newItem);
                    break;
                case SyncList<Transform>.Operation.OP_REMOVEAT:
                    RemoveCameraTarget(index);
                    break;
                default:
                    break;
            }
        }

        private void SetCameraTarget(Transform target)
        {
            m_CameraControl.m_Targets.Add(target);
        }

        private void RemoveCameraTarget(int index)
        {
            m_CameraControl.m_Targets.RemoveAt(index);
        }

        [Server]
        private void CreateNPCs()
        {
            if (!isServer) return;

            for (int i = 0; i < npcEnemyCount; i++)
            {
                var spawnPos = RandomPosition();
                var spawnRot = RandomQuaternion();
                spawnOccupied = true;

                while (spawnOccupied)
                {
                    var hitColliders = Physics.OverlapSphere(spawnPos, 2);
                    if (hitColliders.Length > 1) // If hit with more than ground, get another random position
                        spawnPos = RandomPosition();
                    else
                    {
                        GameObject go = Instantiate(enemyPrefab, spawnPos, spawnRot);
                        npc_Tanks.Add(go);
                        spawnOccupied = false;
                        // Uncomment when server its ready
                        NetworkServer.Spawn(go);
                    }
                }
            }
        }

        [Server]
        private void CreateBoss()
        {
            if (!isServer) return;
            var spawnPos = RandomPosition();
            var spawnRot = RandomQuaternion();
            spawnOccupied = true;

            while (spawnOccupied)
            {
                var hitColliders = Physics.OverlapSphere(spawnPos, 2);
                if (hitColliders.Length > 1) // If hit with more than ground, get another random position
                    spawnPos = RandomPosition();
                else
                {
                    GameObject go = Instantiate(bossPrefab, spawnPos, spawnRot);
                    spawnOccupied = false;
                    // Uncomment when server its ready
                    NetworkServer.Spawn(go);
                }
            }
        }


        [Server]
        private void DeleteNpcs()
        {
            for (int i = 0; i < npc_Tanks.Count; i++)
            {
                int index = cameraTargets.FindIndex(tank => tank == npc_Tanks[i].transform);
                cameraTargets.RemoveAt(index);
                Destroy(npc_Tanks[i]);
            }
            npc_Tanks.Clear();
        }

        /*
        [ServerCallback]
        public void AddPlayerKill(Transform player)
        {
            if (!isServer) return;

            TankManager manager = m_Tanks.Find(tank => tank.m_Instance.transform == player);
            manager.m_RoundKills++;            
        }

        */
        [Server]
        private void UpdateTeamScore(Color teamColor)
        {
            if (!isServer) return;

            if (teamColor == Color.red)
            {
                redTeamScore++;
                redTeamScoreText.SetText("Points: " + redTeamScore.ToString());
            }
            else if (teamColor == Color.blue)
            {
                blueTeamScore++;
                blueTeamScoreText.SetText("Points: " + blueTeamScore.ToString());
            }
            UpdateClientTeamScore(blueTeamScore, redTeamScore);
        }

        [ClientRpc]
        private void UpdateClientTeamScore(int blueScore, int redScore)
        {
            redTeamScoreText.SetText("Points: " + redScore.ToString());
            blueTeamScoreText.SetText("Points: " + blueScore.ToString());
        }

        public bool GetTanksSameColors(Transform player1, Transform player2)
        {
            TankManager manager1 = m_Tanks.Find(tank => tank.m_Instance.transform == player1);
            TankManager manager2 = m_Tanks.Find(tank => tank.m_Instance.transform == player2);

            return manager1.m_PlayerColor == manager2.m_PlayerColor;
        }

        public void SetTankDeaths(Transform player)
        {
            TankManager manager = m_Tanks.Find(tank => tank.m_Instance.transform == player);
            manager.m_RoundKills++;

            UpdateTeamScore(manager.m_PlayerColor);
        }

        [Server]
        private void ResetRoundKills()
        {
            m_Tanks.ForEach(tank => tank.m_RoundKills = 0);
        }

        [ServerCallback]
        public void AddPlayerTank(GameObject playerTank, Color playerColor, string playerName)
        {
            actualPlayerOnGame++;

            TankManager tank = new TankManager();
            tank.m_Instance = playerTank;
            tank.m_PlayerColor = playerColor;
            tank.m_PlayerName = playerName;
            tank.m_PlayerNumber = actualPlayerOnGame;
            tank.Setup();

            m_Tanks.Add(tank);
            //QUITAR
            //SetPointsToWinner(100, tank.m_PlayerName);
            SetTeamPanelText();
            // Game starts on two players connected
            if (actualPlayerOnGame >= 4)
            {
                CheckColorToStart();
            }
        }

        [Server]
        private void CheckColorToStart()
        {
            int bluePlayers = m_Tanks.FindAll(tanks => tanks.m_PlayerColor == Color.blue).Count;
            int redPlayers = m_Tanks.FindAll(tanks => tanks.m_PlayerColor == Color.red).Count;

            if (bluePlayers == 2 && redPlayers == 2)
            {
                OnStartGame?.Invoke();
                InvokeColorsFromTeam();
                StartGame();
            }
        }

        [ClientRpc]
        public void InvokeColorsFromTeam()
        {
            OnStartGame?.Invoke();
        }

        [ServerCallback]
        public void UpdatePlayerTankInfo(GameObject playerTank, Color playerColor, string playerName)
        {
            TankManager tank = m_Tanks.Find(tank => tank.m_Instance == playerTank);
            if (tank != null)
            {
                blueTeam.text = "";
                redTeam.text = "";
                if (playerColor != Color.clear)
                    tank.SetTankColorValue(playerColor);
                Debug.Log(playerName);
                if (playerName != "") tank.SetTankNameValue(playerName);

                CheckColorToStart();
                SetTeamPanelText();
            }
        }

        [Server]
        public void RemovePlayerFromCam()
        {
            TankManager tank = m_Tanks.Find(tank => tank.m_Instance == null);

            if (tank != null)
            {
                m_Tanks.Remove(tank);
                actualPlayerOnGame--;

                for (int i = 0; i < cameraTargets.Count; i++)
                {
                    if (cameraTargets[i] == null)
                    {
                        cameraTargets.RemoveAt(i);
                    }
                }
            }
        }

        [Server]
        private void AddPlayersToCamList()
        {
            if (!isServer) return;

            m_Tanks.ForEach(tank => cameraTargets.Add(tank.m_Instance.transform));
        }

        [Server]
        private void AddNPCsToCamList()
        {
            if (!isServer) return;

            npc_Tanks.ForEach(npc => cameraTargets.Add(npc.transform));
            rocket_Tanks.ForEach(npc => cameraTargets.Add(npc.transform));
        }

        private Vector3 RandomPosition()
        {
            return new Vector3(Random.Range(-30.0f, 30.0f), 0.0f, Random.Range(-30.0f, 30.0f));
        }

        private Quaternion RandomQuaternion()
        {
            return Quaternion.Euler(0.0f, Random.Range(0, 360), 0.0f);
        }

        public void SetWaitingText()
        {
            m_MessageWaitingText.text = "Waiting more players...";
        }

        private void StopGame()
        {
            if (isLocalPlayer)
            {
                OnPlayerStopGame?.Invoke();
            }
            else if (isServer)
            {
                StartCoroutine(StopServer());

                RptDestroyMatchmaking();
            }
        }

        private IEnumerator StopServer()
        {
            yield return new WaitForSeconds(1f);
            OnServerStopGame?.Invoke();
        }

        [Server]
        private IEnumerator GameLoop()
        {
            // Start off by running the 'RoundStarting' coroutine but don't return until it's finished
            yield return StartCoroutine(RoundStarting());

            // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished
            yield return StartCoroutine(RoundPlaying());

            // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished
            yield return StartCoroutine(RoundEnding());

            // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found
            if (m_GameWinner != null)
            {
                // If there is a game winner, restart the level
                //SceneManager.LoadScene(0);
                StopGame();
            }
            else
            {
                // If there isn't a winner yet, restart this coroutine so the loop continues
                // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end
                ResetRoundKills();
                CreateNPCs();
                AddNPCsToCamList();
                StartCoroutine(GameLoop());
            }
        }

        [Server]
        private IEnumerator RoundStarting()
        {
            // As soon as the round starts reset the tanks and make sure they can't move
            ResetAllTanks();
            DisableTankControl();

            redTeamScore = 0;
            blueTeamScore = 0;
            UpdateTeamScore(Color.clear);

            // Snap the camera's zoom and position to something appropriate for the reset tanks
            m_CameraControl.SetStartPositionAndSize();

            // Increment the round number and display text showing the players what round it is
            m_RoundNumber++;
            string roundText = "ROUND " + m_RoundNumber;
            m_MessageText.text = roundText;
            SetClientText(roundText);

            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_StartWait;
        }

        [Server]
        private IEnumerator RoundPlaying()
        {
            // As soon as the round begins playing let the players control the tanks
            EnableTankControl();

            // Clear the text from the screen
            m_MessageText.text = string.Empty;
            m_MessageWaitingText.text = string.Empty;
            SetClientText(string.Empty);

            // While there is not one tank left...
            while (!NoNPCsLeft())
            {
                // ... return on the next frame
                yield return null;
            }
        }

        [Server]
        private IEnumerator RoundEnding()
        {
            DeleteNpcs();
            // Stop tanks from moving
            DisableTankControl();

            if (!teamMatch)
            {
                // Clear the winner from the previous round
                m_RoundWinner = null;

                // See if there is a winner now the round is over
                m_RoundWinner = GetRoundWinner();

                // If there is a winner, increment their score
                if (m_RoundWinner != null)
                {
                    m_RoundWinner.m_Wins++;
                }

                // Now the winner's score has been incremented, see if someone has one the game
                m_GameWinner = GetGameWinner();
                GameFinished = m_GameWinner != null;

                // Get a message based on the scores and whether or not there is a game winner and display it
                string message = EndMessage();
                m_MessageText.text = message;
                SetClientText(message);
            }
            else
            {
                // Clear the winner from the previous round
                m_TeamRoundWinner = Color.black;

                // See if there is a team winner now the round is over
                m_TeamRoundWinner = GetTeamRoundWinner();
                UpdateTeamRound(m_TeamRoundWinner);

                // If there is a team winner, increment their score
                if (m_TeamRoundWinner != Color.black)
                {
                    foreach (TankManager tankInstance in m_Tanks)
                    {
                        if (tankInstance.m_PlayerColor == m_TeamRoundWinner)
                            tankInstance.m_Wins++;
                        Debug.Log("Victorias: " + tankInstance.m_Wins);
                    }
                }
                // Now the winner's score has been incremented, see if someone has one the game
                m_GameWinner = GetGameWinner();
                GameFinished = m_GameWinner != null;

                // Get a message based on the scores and whether or not there is a game winner and display it
                string message = EndMessage();
                m_MessageText.text = message;
                SetClientText(message);
            }
            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_EndWait;
        }

        [ClientRpc]
        private void RptDestroyMatchmaking()
        {
            GameObject.Destroy(GameObject.Find("MatchmakingManager"));
        }

        [ClientRpc]
        private void SetClientText(string text)
        {
            m_MessageText.text = text;
            m_MessageWaitingText.text = string.Empty;
        }

        [Server]
        private void SetTeamPanelText()
        {
            string blueText = "";
            string redText = "";

            foreach (TankManager tankInstance in m_Tanks)
            {
                if (tankInstance.m_PlayerColor == Color.blue)
                    blueText += tankInstance.m_PlayerName + "\n";
                if (tankInstance.m_PlayerColor == Color.red)
                    redText += tankInstance.m_PlayerName + "\n";
            }
            blueTeam.text = blueText;
            redTeam.text = redText;

            SetClientTeamPanelText(blueText, redText);
        }

        [ClientRpc]
        private void SetClientTeamPanelText(string blueText, string redText)
        {
            blueTeam.text = blueText;
            redTeam.text = redText;
        }

        // This is used to check if there is one or fewer tanks remaining and thus the round should end
        [Server]
        private bool NoNPCsLeft()
        {
            // Start the count of tanks left at zero.
            int npcsLeft = 0;

            // Go through all the tanks...
            for (int i = 0; i < npc_Tanks.Count; i++)
            {
                // ... and if they are active, increment the counter.
                if (npc_Tanks[i].activeSelf)
                    npcsLeft++;
            }

            // If there are one or fewer tanks remaining return true, otherwise return false.
            return npcsLeft == 0;
        }

        // This function is to find out if there is a winner of the round
        // This function is called with the assumption that 1 or fewer tanks are currently active
        [Server]
        private TankManager GetRoundWinner()
        {
            // Go through all the tanks...
            TankManager winner = m_Tanks[0];
            int enemyCount = winner.m_RoundKills;

            for (int i = 1; i < m_Tanks.Count; i++)
            {
                // Tank with more kill, wins the round
                if (m_Tanks[i].m_RoundKills > enemyCount)
                {
                    winner = m_Tanks[i];
                    enemyCount = winner.m_RoundKills;
                }
                // On same kills, we dont update enemyCount
                else if (m_Tanks[i].m_RoundKills == enemyCount)
                {
                    winner = null;
                }
            }

            // If none of the tanks are active it is a draw so return null
            return winner;
        }


        //TEAMWORK
        // This function is to find out if there is a team winner of the round
        [Server]
        private Color GetTeamRoundWinner()
        {
            int blueTeamPoints = 0;
            int redTeamPoints = 0;
            Color winner = Color.black;
            foreach (TankManager tankInstance in m_Tanks)
            {
                if (tankInstance.m_PlayerColor == Color.blue)
                    blueTeamPoints += tankInstance.m_RoundKills;
                if (tankInstance.m_PlayerColor == Color.red)
                    redTeamPoints += tankInstance.m_RoundKills;
            }
            if (blueTeamPoints > redTeamPoints)
            {
                winner = Color.blue;
            }
            else if (blueTeamPoints < redTeamPoints)
            {
                winner = Color.red;
            }
            return winner;
        }

        [Server]
        private void UpdateTeamRound(Color teamColor)
        {
            if (!isServer) return;

            if (teamColor == Color.red)
            {
                redRoundScore++;
                redRoundText.SetText(redRoundScore.ToString());
            }
            else if (teamColor == Color.blue)
            {
                blueRoundScore++;
                blueRoundText.SetText(blueRoundScore.ToString());
            }
            UpdateClientTeamRound(blueRoundScore, redRoundScore);
        }

        [ClientRpc]
        private void UpdateClientTeamRound(int blueScore, int redScore)
        {
            redRoundText.SetText(redScore.ToString());
            blueRoundText.SetText(blueScore.ToString());
        }

        // This function is to find out if there is a winner of the game
        [Server]
        private TankManager GetGameWinner()
        {
            int winners = 0;
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                // ... and if one of them has enough rounds to win the game, return it
                if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                {
                    SetPointsToWinner(100, m_Tanks[i].m_PlayerName);
                    winners++;
                    if (winners == 2)
                    {
                        return m_Tanks[i];
                    }
                }
            }

            // If no tanks have enough rounds to win, return null
            return null;
        }

        [ClientRpc]
        private void SetPointsToWinner(int points, string playerName)
        {
            // 09/06/2023
            GameObject.Find("PlayFab").GetComponent<PlayFabController>().SubmitScore(points, playerName);
            //            m_Tanks[i].SubmitScore(100);
        }

        // Returns a string message to display at the end of each round
        [Server]
        private string EndMessage()
        {
            // By default when a round ends there are no winners so the default end message is a draw
            string message = "DRAW!";

            if (!teamMatch)
            {
                // If there is a winner then change the message to reflect that
                if (m_RoundWinner != null)
                    message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";
            }
            else
            {
                //AQU?!
                if (m_TeamRoundWinner == Color.blue)
                    message = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.blue) + ">" + "TEAM BLUE WINS THE ROUND" + "</color>";
                else if (m_TeamRoundWinner == Color.red)
                    message = "<color=#" + ColorUtility.ToHtmlStringRGB(Color.red) + ">" + "TEAM RED WINS THE ROUND" + "</color>";
            }

            // Add some line breaks after the initial message
            message += "\n\n";

            // Go through all the tanks and add each of their scores to the message
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
            }

            // If there is a game winner, change the entire message to reflect that

            if (m_GameWinner != null)
            {
                if (!teamMatch)
                {
                    message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";
                }
                else
                {
                    if (m_GameWinner.m_PlayerColor == Color.blue)
                    {
                        message = "<color=#" + ColorUtility.ToHtmlStringRGB(m_GameWinner.m_PlayerColor) + ">" + "TEAM BLUE WINS THE GAME" + "</color>";
                    }
                    else if (m_GameWinner.m_PlayerColor == Color.red)
                    {
                        message = "<color=#" + ColorUtility.ToHtmlStringRGB(m_GameWinner.m_PlayerColor) + ">" + "TEAM RED WINS THE GAME" + "</color>";
                    }
                }
            }

            return message;
        }

        // This function is used to turn all the tanks back on and reset their positions and properties
        private void ResetAllTanks()
        {
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                m_Tanks[i].Reset();
            }
        }

        private void EnableTankControl()
        {
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                m_Tanks[i].EnableControl();
            }
        }

        private void DisableTankControl()
        {
            for (int i = 0; i < m_Tanks.Count; i++)
            {
                m_Tanks[i].DisableControl();
            }
        }
    }
}